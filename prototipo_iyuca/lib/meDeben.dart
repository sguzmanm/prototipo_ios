import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'model/app_state_model.dart';

class ShoppingCartTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var spacecrafts = ["Don Jacinto 3 KG Fresas","Doña Imelda 4 Unidades Guayaba","Pancracio 5 Litros Cerveza","Tulio Triviño 2 KG Papa"];
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(

        backgroundColor: CupertinoColors.activeGreen,
        middle: Text("Lo que me deben",style: TextStyle(fontSize: 25)),
      ),

      child: Container(
          child: new ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.blueGrey,
            ),
            padding: new EdgeInsets.fromLTRB(0.0,20.0,0.0,20.0),
            itemCount: spacecrafts.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 50,
                child: Center(child: Text('${spacecrafts[index]}')),
              );
            },


          )
      ),
    );


  }

}