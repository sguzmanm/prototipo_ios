import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:prototipo_iyuca/confirmacion.dart';

class Iniciar extends StatefulWidget {

  final FlutterBlue flutterBlue = FlutterBlue.instance;
  final List<BluetoothDevice> devicesList = new List<BluetoothDevice>();

  @override
  State<StatefulWidget> createState() => new Buscando();
}

class Buscando extends State<Iniciar> {

  BluetoothDevice _connectedDevice;
  List<BluetoothService> _services;
  StreamSubscription _stream;

  @override
  void initState() {

    super.initState();
    widget.flutterBlue.connectedDevices
        .asStream()
        .listen((List<BluetoothDevice> devices) {
      for (BluetoothDevice device in devices) {
        _addDeviceTolist(device);
      }
    });
    _stream = widget.flutterBlue.scanResults.listen((List<ScanResult> results) {
      for (ScanResult result in results) {
        _addDeviceTolist(result.device);
      }
    });
    //Se empieza a buscar
    widget.flutterBlue.startScan();
    widget.flutterBlue.stopScan();
    for(BluetoothDevice device in widget.devicesList){
      connectToDevice(device);
    }
    startTime();
  }

  //Agrega a la lista
  _addDeviceTolist(final BluetoothDevice device) {
    if (!widget.devicesList.contains(device)) {
      setState(() {
        widget.devicesList.add(device);
        print("El tamaño de la lista de dispositivos es " + widget.devicesList.length.toString());
        print("Se añadio a la lista el siguiente dispositivo" + device.id.toString());
        //connectToDevice(device);
      });
    }
  }

  //Se supone que con esto debe conectarse a los dispositivos de la lista
  connectToDevice ( BluetoothDevice device ) async{

      print("el dispositivo es " + device.id.toString());

      try {
        print("ESTOY EN EL TRY");
        print("VOY A CONECTARME A " + device.id.toString());
        //Se supone que esto lo conecta.
        await device.connect();

        _services = await device.discoverServices();
        var state=  device.state;
        var lista = await state.toList();


        print("El tamaño de los servicios es :");
        print(_services.length);
        print("Los servicios son :");
        print(_services);
      } catch (e) {
        print("ESTOY EN EL CATCH");
        //Si ya está conectado lanza esta excepcion
        if (e.code != 'already_connected') {
          throw e;
        }
      } finally {
        print("ESTOY EN EL FINALLY");
        //No sé que hace jajaj
        _services = await device.discoverServices();
      }

      for(BluetoothService service in _services){
        print(service.uuid);
        for(BluetoothCharacteristic c in service.characteristics){
          print(c.uuid);
          print(c.toString());
        }
      }

      _connectedDevice = device;
      print("Se supone que me conecté a :"+ _connectedDevice.id.toString());
      print("Me voy a desconectar");
      device.disconnect();
      print("Se supone que desconectó");
      print("valor de _connectedDevice " +  _connectedDevice.toString());

      widget.flutterBlue.stopScan();
  }

  startTime() async {
    var duration = new Duration(seconds: 12);
    return new Timer(duration, route);
  }

  route() {
    Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context) => Confirmacion()
    )
    );
  }

  buscar() async{

    final prefs = await SharedPreferences.getInstance();

    // Try reading data from the counter key. If it doesn't exist, return 0.
    var counter = prefs.getString('producto') ?? 0;
    var user = prefs.getString('usuario') ?? 0;

    print(counter);
    print("Usuario: ");
    print(user);

  }

  cancelar(context) async{
    final prefs = await SharedPreferences.getInstance();

    prefs.remove('producto');
    prefs.remove('cantidad');
    prefs.remove('medida');
    Navigator.of(context).pop();
  }

  @override
  dispose(){
    widget.flutterBlue.stopScan();
    widget.devicesList.clear();
    _stream.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
        body: ListView(children: <Widget>[
          Container(
              margin: EdgeInsets.fromLTRB(0,100, 0, 0),
              height: 400,
              child: Image.network(
            'https://gifimage.net/wp-content/uploads/2017/11/frutas-y-verduras-gif-12.gif',
          )),
          Container(
              margin: EdgeInsets.fromLTRB(0,50, 0, 0),
              child: Center(
                  child : Text('Estamos buscando quien te preste',
                      style: TextStyle(fontSize: 25, color: Colors.grey,))),
              ),
          Container(
              //margin: EdgeInsets.fromLTRB(0,50, 0, 0),
              child: CupertinoButton(
                  minSize: 50,
                  child: Text("Cancelar",
                      style: TextStyle(
                        fontSize: 20,
                      )),
                onPressed: () {
                  buscar();
                  cancelar(context);
                },
                  color: CupertinoColors.systemRed,
                ))
    ]));
  }
}
