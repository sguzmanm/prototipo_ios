import 'package:flutter/cupertino.dart';
import 'package:prototipo_iyuca/agitar.dart';
import 'package:prototipo_iyuca/buscando.dart';
import 'package:prototipo_iyuca/confirmacion.dart';
import 'package:prototipo_iyuca/favorResgistrado.dart';
import 'package:prototipo_iyuca/services/authentication.dart';
import 'package:flutter/material.dart';
import 'hacerPedido.dart';   // NEW
import 'debo.dart';         // NEW
import 'meDeben.dart';  // NEW
import 'rootPage.dart';


class CupertinoStoreApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {



    return MaterialApp(
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
      ),
      routes: {
        'pedido': (context) => hacerPedido(),
        'buscando': (context) => Iniciar(),
        'confirmacion': (context) => Confirmacion(),
        'agitar': (context) => Agitar(),
        'registrado': (context) => FavorRegistrado(),
        'debo' :(context) => SearchTab(),
      },
      home: new RootPage(auth: new Auth()),
    );

  }
}
class CupertinoStoreHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(

        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.shopping_cart,
            color: CupertinoColors.systemYellow,),
            title: Text('Hacer Pedido', style: TextStyle(color: CupertinoColors.systemYellow),),
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.pen,
            color: CupertinoColors.systemRed,),

            title: Text('Lo que debo'),
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.pen,
            color: CupertinoColors.systemGreen,),
            title: Text('Lo que me deben'),
          ),
        ],
      ),
      tabBuilder: (context, index) {
        switch (index) {
          case 0:
            return CupertinoTabView(builder: (context) {
              return CupertinoPageScaffold(
                child: hacerPedido(),

              );
            });
          case 1:
            return CupertinoTabView(builder: (context) {
              return CupertinoPageScaffold(
                child: SearchTab(),
              );
            });
          case 2:
            return CupertinoTabView(builder: (context) {
              return CupertinoPageScaffold(
                child: ShoppingCartTab(),
              );
            });
        }
      },
    );
  }
}