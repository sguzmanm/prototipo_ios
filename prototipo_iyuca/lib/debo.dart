import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:prototipo_iyuca/hacerPedido.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

class SearchTab extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => new Debo();
}

class Debo extends State<SearchTab> {

  var spacecrafts;

  @override
  void initState() {


    var x = nuevoValor();

    spacecrafts = ["Don Jacinto 6 KG Papa","Doña Imelda 12 Unidades Cebolla","Pancracio 20 Unidades Manzana","Tulio Triviño 13 KG Mango",x];

    super.initState();
  }

  nuevoValor() async {

    final prefs = await SharedPreferences.getInstance();

    // Try reading data from the counter key. If it doesn't exist, return 0.

    var futures = List<Future>();

    futures.add(prefs.getString('producto') ?? 0);
    futures.add(prefs.getString('usuario') ?? 0);
    futures.add(prefs.getInt('cantidad').toString() ?? 0);
    futures.add(prefs.getString('medida') ?? 0);





    var tot= '';
    for (Future i in futures){
      i.then((value) => tot += " "+ value);
    }

    await Future.wait(futures);
    Future.delayed(new Duration(seconds: 2));
    return tot;
  }


  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: AppBar(
        backgroundColor: CupertinoColors.activeGreen,
        title: Text("Lo que debo",style: TextStyle(fontSize: 25)),
      ),

      body: Container(
        child: new ListView.separated(
          separatorBuilder: (context, index) => Divider(
            color: Colors.blueGrey,
          ),
          padding: new EdgeInsets.fromLTRB(0.0,20.0,0.0,20.0),
          itemCount: spacecrafts.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              height: 50,
              child: Center(child: Text('${spacecrafts[index]}')),
            );
          },


        )
      ),
    );


  }


}
