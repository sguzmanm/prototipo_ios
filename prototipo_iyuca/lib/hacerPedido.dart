import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prototipo_iyuca/buscando.dart';
import 'package:prototipo_iyuca/model/product.dart';
import 'package:prototipo_iyuca/styles.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:prototipo_iyuca/services/authentication.dart';
import 'package:prototipo_iyuca/model/products_repository.dart';

class hacerPedido extends StatefulWidget{
  hacerPedido({Key key, this.auth, this.userId, this.logoutCallback})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  @override
  State<StatefulWidget> createState() => _ProductListTab();

}


class _ProductListTab extends State<hacerPedido> {
  final _textController = TextEditingController();

  String _producto;
  int _cantidad;
  String _medida;



  @override
  void initState(){
    _producto = null;
    _cantidad = 0;
    _medida = null;

  }

  @override
  void dispose(){
    _textController.dispose();

    super.dispose();
  }


  _guardarValores(context) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

// set value
    prefs.setString('usuario', widget.userId);
    prefs.setString('producto', _producto);
    prefs.setInt('cantidad', int.parse(_textController.text));
    prefs.setString('medida', _medida);
    Navigator.pushNamed(context, 'buscando');
  }


  @override
  Widget build(BuildContext context) {

    var lista = ProductsRepository.loadProducts(Category.all);
    var medidas = ['KG', 'Unidades', 'Litros', 'Gramos'];

    return Scaffold(
          appBar: AppBar(
            backgroundColor: CupertinoColors.activeGreen,
            title: Text("Hacer Pedido",style: TextStyle(fontSize: 25)),
          ),

        body: ListView(

          padding: const EdgeInsets.all(8),
          children: <Widget>[
            Container(
              height: 20,
              margin: EdgeInsets.fromLTRB(0,120, 0, 0),
              child: const Center(child: Text('Escoja el producto a pedir', style: TextStyle(fontSize: 20,color: Colors.black ), )),
            ),
            Container(
                height: 100,
                child: CupertinoPicker(itemExtent: 50,
                    scrollController: FixedExtentScrollController(initialItem: 1),
                    backgroundColor: Colors.white,
                    onSelectedItemChanged: (i) => setState(() => _producto = lista[i].name),
                    children: lista != null ? lista.map((prod) {

                        return new Text(
                          prod.name,
                          textAlign: TextAlign.center,
                          style: new TextStyle(
                            fontSize: 20.0,

                        ),
                      );
                    }
                    ).toList():[]

                )
            ),
            Container(
              height: 20,
              margin: EdgeInsets.fromLTRB(0,30, 0, 0),
              child: const Center(child: Text('Cantidad', style: TextStyle(fontSize: 20, color: Colors.black),)),
            ),
            Container(
              height: 50,
              margin: EdgeInsets.fromLTRB(0,10, 0, 0),
              child: Center(
                  child: CupertinoTextField(
                      controller: _textController,
                      keyboardType: TextInputType.numberWithOptions(signed: false, decimal: false) ,
                  ),
              ),
            ),
            Container(
              height: 20,
              margin: EdgeInsets.fromLTRB(0,50, 0, 0),
              child: const Center(child: Text('Unidad', style: TextStyle(fontSize: 20, color: Colors.black),)),
            ),
            Container(
                height: 100,
                child: CupertinoPicker(itemExtent: 50,
                    scrollController: FixedExtentScrollController(initialItem: 1),
                    backgroundColor: Colors.white,
                    onSelectedItemChanged: (i) => setState(() => _medida = medidas[i]),
                    children: medidas != null ? medidas.map((med) {

                        return new Text(
                          med,
                          textAlign: TextAlign.center,
                          style: new TextStyle(
                            fontSize: 20.0,
                          ),

                      );
                    }
                    ).toList():[]
                )
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0,50, 0, 0),
              child: CupertinoButton(
                minSize: 50,
                child: Text("Pedir", style: TextStyle(fontSize: 20, )),
                onPressed: () => _guardarValores(context),
                color: CupertinoColors.activeGreen,
              ),
            )
          ],
        )
    );


  }

}