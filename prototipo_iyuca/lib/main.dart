import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'app.dart';
import 'model/app_state_model.dart';
import 'dart:isolate';
import 'package:flutter_blue/flutter_blue.dart';

void main() {
  // This app is designed only to work vertically, so we limit
  // orientations to portrait up and down.
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  initIsolate();
  return runApp( CupertinoStoreApp());
}

Future<SendPort> initIsolate() async {
  Completer completer = new Completer<SendPort>();
  ReceivePort isolateToMainStream = ReceivePort();

  isolateToMainStream.listen((data) {
    if (data is SendPort) {
      SendPort mainToIsolateStream = data;
      completer.complete(mainToIsolateStream);
    } else {
      print('[isolateToMainStream] $data');
    }
  });

  Isolate myIsolateInstance = await Isolate.spawn(myIsolate, isolateToMainStream.sendPort);
  return completer.future;
}

void myIsolate(SendPort isolateToMainStream) async {
  final FlutterBlue flutterBlue = FlutterBlue.instance;

  ReceivePort mainToIsolateStream = ReceivePort();
  isolateToMainStream.send(mainToIsolateStream.sendPort);

  mainToIsolateStream.listen((data) {
    print('[mainToIsolateStream] $data');
    //exit(0);
  });

  isolateToMainStream.send('This is from myIsolate()');


  var stream = flutterBlue.state;

  var suscription = stream.listen(
      (data){
        print(data);
        if(data == 'hola'){

        }
      }
  );

  //await characteristic.setNotifyValue(true);
  //characteristic.value.listen((value) {
    // do something with new value
 // });

  suscription.cancel();
}