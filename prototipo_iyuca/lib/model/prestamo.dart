import 'product.dart';
import 'package:flutter/foundation.dart';


class Prestamo {
  const Prestamo({
    @required this.producto,
    @required this.cantidad,
    @required this.medida,
    @required this.prestador,
    @required this.idPrestador,
  })  : assert(producto != null),
        assert(cantidad != null),
        assert(medida != null),
        assert(prestador != null),
        assert(idPrestador != null);

  final Product producto;
  final int cantidad;
  final String medida;
  final String prestador;
  final int idPrestador;

}